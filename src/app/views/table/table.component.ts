import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { TableService } from 'src/app/shared/services/table.service';
import { Table } from 'src/app/shared/models/table';
import { Toast, generateSwal } from 'src/app/shared/utils/design';
import { ToastrService } from 'ngx-toastr';
import { CODE_ERROR } from 'src/app/shared/utils/code.error';
import { handleError } from 'src/app/shared/utils/utils';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  table: Table = new Table();
  loading: boolean = true;
  username: string = null;
  error: boolean = false;
  error_message: string;
  private toast: Toast = new Toast(this.toastService);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _tableService: TableService,
    private toastService: ToastrService
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.table._id = params.get('id');
      this.checkDisponibilty();
    });
  }

  isDispo(): boolean {
    if (!this.table.available) {
      this.error_message = CODE_ERROR.TABLE_CLOSED;
      return false;
    }
    if (this.table.current >= this.table.max) {
      this.error_message = CODE_ERROR.TABLE_MAX_CAPACITY;
      return false;
    }
    return true;
  }

  checkDisponibilty(): void {
    this._tableService.checkDisponibility(this.table._id).subscribe(
      (data) => {
        this.table.available = data['available'];
        this.table.min = data['min'];
        this.table.max = data['max'];
        this.table.current = data['current'];
        if (!this.isDispo()) {
          this.error = true;
        }
        this.loading = false;
      },
      (err: any) => {
        this.loading = false;
        const messError = handleError(err.error);
        if (
          [
            CODE_ERROR.TABLE_NOT_FOUND,
            CODE_ERROR.TABLE_CLOSED,
            CODE_ERROR.TABLE_MAX_CAPACITY,
          ].includes(messError)
        ) {
          this.error = true;
          this.error_message = messError;
        } else {
          this.toast.generateToast('error', 'Erreur !', messError);
        }
      }
    );
  }

  joinTable() {
    if (this.username) {
      this._tableService.joinTable(this.table._id, this.username).subscribe(
        (data) => {
          this.joinProcess(data);
        },
        (err: any) => {
          this.joinFailToast(handleError(err.error));
        }
      );
    } else {
      this.toast.generateToast(
        'error',
        'Erreur',
        "Le nom d'utilisateur doit contenir 2 caractères"
      );
    }
  }

  joinProcess(data: Table): void {
    if (
      this._tableService.setUser(this.username, this.table._id, data.restaurant)
    ) {
      generateSwal(
        `Bienvenue ${this.username}, vous avez rejoint la table`,
        'success'
      );
      this.router.navigate(['/card/products']);
    } else {
      const error_message = CODE_ERROR.OTHER_ERROR;
      this.joinFailToast(error_message);
    }
  }

  joinFailToast(body: string): void {
    this.toast.generateToast('error', 'Impossible de rejoindre la table', body);
  }
}
