import { Toast } from './../../shared/utils/design';
import { ToastrService } from 'ngx-toastr';
import { generateSwal } from 'src/app/shared/utils/design';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { interval } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/shared/services/order.service';
import { Order } from 'src/app/shared/models/order';
import { config } from 'config';
import { Customer } from 'src/app/shared/models/customer';
import { TableService } from 'src/app/shared/services/table.service';
import { constants } from 'src/app/shared/utils/constants';
import { handleError } from 'src/app/shared/utils/utils';

enum UserAction {
  CANCEL,
  DECLINE,
}

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {
  /**
   * TODO
   * Améliorer l'afficage des expend panel
   */
  userAction = UserAction;
  orders: Order[] = [];
  loading: boolean = true;
  customer: Customer;
  public BASE_API_URL = config.BASE_API_URL;
  private toast: Toast = new Toast(this.toastService);

  constructor(
    private orderService: OrderService,
    private tableService: TableService,
    private toastService: ToastrService
  ) {
    this.customer = this.tableService.getUser();
  }

  getTitle(): string {
    if (this.customer.master) return 'Les commandes de la table';
    else return 'Mes commandes';
  }

  private getCustomerInfo(): void {
    this.tableService.getCustomerInfo().subscribe(
      (data: Customer) => {
        this.customer.master = data.master ?? false;
        this.orderRefresh();
      },
      (err) => {
        generateSwal(handleError(err.error), 'error');
        this.loading = false;
      }
    );
  }
  ngOnInit(): void {
    this.getCustomerInfo();
  }

  private declineOrder(order: Order) {
    this.orderService.declineOrder(order).subscribe(
      () =>
        this.toast.generateToast(
          'success',
          'Succès',
          `Commande de ${order.username} refusée`
        ),
      (err) =>
        this.toast.generateToast(
          'error',
          'Refus de commande impossible',
          handleError(err)
        )
    );
  }

  private cancelOrder(order: Order) {
    this.orderService.cancelOrder(order).subscribe(
      () => this.toast.generateToast('success', 'Succès', 'Commande annulée'),
      (err) =>
        this.toast.generateToast(
          'error',
          'Annulation de commande impossible',
          handleError(err)
        )
    );
  }

  private askAction(
    title: string,
    text: string,
    action: UserAction,
    order: Order,
    type: string = 'success'
  ): void {
    Swal.fire({
      title: title,
      text: text,
      icon: type,
      showCancelButton: true,
      confirmButtonText: 'Oui',
      confirmButtonColor: config.THEME.primary,
      cancelButtonText: 'Annuler',
      cancelButtonColor: config.THEME.error,
    }).then((result: any) => {
      if (result.value) {
        this.doActions(action, order);
      }
    });
  }

  performAction(action: UserAction, order: Order) {
    let title = '';
    let text = '';
    let icon = 'question';
    switch (action) {
      case this.userAction.CANCEL:
        title = 'Annulation de commande';
        text = 'Êtes-vous sur de vouloir annuler votre commande ?';
        break;
      case this.userAction.DECLINE:
        title = 'Refuser une commande';
        text = 'Êtes-vous sur de vouloir refuser cette commande ?';
        break;
    }
    this.askAction(title, text, action, order, icon);
  }

  private doActions(action: UserAction, order: Order) {
    switch (action) {
      case this.userAction.CANCEL:
        this.cancelOrder(order);
        break;
      case this.userAction.DECLINE:
        this.declineOrder(order);
    }
  }

  private orderRefresh(): void {
    interval(1000)
      .pipe(
        flatMap(() => {
          if (this.customer.master) {
            return this.orderService.getAllOrdersOfTable();
          } else {
            return this.orderService.getAllOrders();
          }
        })
      )
      .subscribe(
        (data: Order[]) => {
          this.orders = data;
          this.loading = false;
        },
        (err) => {
          generateSwal(handleError(err.error), 'error');
          this.loading = false;
        }
      );
  }

  getUsername(username: string): string {
    if (username === this.customer.username) {
      return `${username} (moi)`;
    }
    return username;
  }

  getStatusMessage(status: string): string {
    return constants.ORDER_STATUS[status]?.message ?? 'Indéfini';
  }
  getStatusIcon(status: string): string {
    return constants.ORDER_STATUS[status]?.icon ?? 'more_vert';
  }
}
