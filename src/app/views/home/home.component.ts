import { Component, OnInit } from '@angular/core';
import { constants } from 'src/app/shared/utils/constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public app_title = constants.APP_TITLE;
  constructor() {}

  ngOnInit(): void {}
}
