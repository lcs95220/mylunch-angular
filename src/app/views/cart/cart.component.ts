import { Customer } from 'src/app/shared/models/customer';
import { Component, OnInit, HostListener } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Toast, generateSwal } from 'src/app/shared/utils/design';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { config } from 'config';
import { TableService } from 'src/app/shared/services/table.service';
import { OrderService } from 'src/app/shared/services/order.service';
import { OrderProduct } from 'src/app/shared/models/order-product';
import { handleError } from 'src/app/shared/utils/utils';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  userProducts: OrderProduct[];
  master: boolean = false;
  loading: boolean = true;
  tableId: string;
  username: string;
  BASE_API_URL: string = config.BASE_API_URL;
  private toast: Toast = new Toast(this.toastService);

  constructor(
    private router: Router,
    private tableService: TableService,
    private orderService: OrderService,
    private toastService: ToastrService
  ) {}
  ngOnInit(): void {
    this.loading = true;
    this.tableId = localStorage.getItem('table_id');
    this.username = localStorage.getItem('username');
    this.userProducts = JSON.parse(localStorage.getItem('user_products')) ?? [];
    this.getCustomerInfo();
  }

  getCustomerInfo(): void {
    this.tableService.getCustomerInfo().subscribe(
      (data: Customer) => {
        this.master = data.master ?? false;
        this.loading = false;
      },
      (err) => {
        this.toast.generateToast('error', 'Erreur !', handleError(err.error));
        this.loading = false;
      }
    );
  }

  askClearCart(): void {
    Swal.fire({
      title: 'Vider le panier',
      text: 'Êtes-vous sur de vouloir vider le panier',
      icon: 'error',
      showCancelButton: true,
      confirmButtonText: 'Oui',
      confirmButtonColor: config.THEME.primary,
      cancelButtonText: 'Annuler',
      cancelButtonColor: config.THEME.error,
    }).then((result: any) => {
      if (result.value) {
        this.clearCart();
      }
    });
  }

  private clearCart(): void {
    localStorage.removeItem('user_products');
    this.userProducts = [];
  }

  createOrder() {
    this.orderService.createOrder(this.userProducts).subscribe(
      () => {
        this.clearCart();
        this.router.navigate(['order']);
        generateSwal('Commande enregistrée', 'success', 'top-end');
      },
      (err) => {
        this.toast.generateToast(
          'error',
          "Echec lors de l'envoie de commande",
          handleError(err.error)
        );
        if (err === 'TABLE_OR_USERNAME_DOES_NOT_EXIST') {
          this.tableService.cleanStorage();
          this.router.navigate(['home']);
        }
      }
    );
  }
  swalValidate(): void {
    let message = {
      title: 'Validation de la commande',
      text: 'Votre commande sera envoyée au maitre de la table',
    };
    if (this.master) {
      message = {
        title: 'Valider mon panier',
        text:
          'Vous allez valider votre panier et accéder aux commandes de la table',
      };
    }
    Swal.fire({
      title: message.title,
      text: message.text,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Oui',
      confirmButtonColor: config.THEME.secondary,
      cancelButtonText: 'Annuler',
      cancelButtonColor: config.THEME.primary,
    }).then((result) => {
      if (result.value) {
        this.createOrder();
      }
    });
  }

  validateOrder(): void {
    this.swalValidate();
  }
}
