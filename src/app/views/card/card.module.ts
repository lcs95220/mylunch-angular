import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardRoutingModule } from './card.routing';
import { CardComponent } from './card.component';
import { ProductComponent } from './product/product.component';
import { AngularMaterialModule } from 'src/app/shared/components/angular-material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [CardComponent, ProductComponent],
  imports: [CommonModule, CardRoutingModule, AngularMaterialModule, NgbModule],
})
export class CardModule {}
