import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/shared/services/product.service';
import { Product } from 'src/app/shared/models/product';
import { OrderProduct } from 'src/app/shared/models/order-product';
import { ProductCategory } from 'src/app/shared/models/product-category';
import { Toast } from 'src/app/shared/utils/design';
import { checkMobile } from 'src/app/shared/utils/utils';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { config } from 'config';
import { Customer } from 'src/app/shared/models/customer';
import { TableService } from 'src/app/shared/services/table.service';
import { Swipe } from 'src/app/shared/utils/swipe';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  constructor(
    private productService: ProductService,
    private toastService: ToastrService,
    private tableService: TableService,
    private router: Router
  ) {}

  private swipe: Swipe = new Swipe();
  public customer: Customer;
  public selectedTab: number = 0;
  public mobile: boolean;
  public products: OrderProduct[] = [];
  public loading: boolean = true;
  public BASE_API_URL = config.BASE_API_URL;
  public error: boolean = false;
  public product_categories: ProductCategory[] = [];
  private toast: Toast = new Toast(this.toastService);

  async ngOnInit() {
    this.loading = true;
    this.mobile = checkMobile();
    this.customer = this.tableService.getUser();
    if (this.customer.restaurantId) {
      this.getAllCategories(this.customer.restaurantId);
      this.getAllProducts(this.customer.restaurantId);
    } else {
      this.loading = false;
    }
  }

  getAllProducts(restaurant_id: string): void {
    this.productService
      .getAllProducts(restaurant_id)
      .subscribe((data: Product[]) => {
        this.products = data.map((product: Product) => {
          return new OrderProduct(product);
        });
        this.checkUserProducts();
        this.loading = false;
      });
  }

  clearCart() {
    Swal.fire({
      title: 'Vider le panier',
      text: 'Êtes-vous sur de vouloir vider le panier',
      icon: 'error',
      showCancelButton: true,
      confirmButtonText: 'Oui',
      confirmButtonColor: '#ff3333',
      cancelButtonText: 'Annuler',
      cancelButtonColor: '#20313b',
    }).then((result: any) => {
      if (result.value) {
        localStorage.removeItem('user_products');
        this.products.map((product) => (product.user_quantity = 0));
      }
    });
  }

  private getProductById(products: OrderProduct[], id: string): Product {
    return products.find(
      (user_product: OrderProduct) => user_product.product._id === id
    );
  }

  private checkUserProducts(): void {
    let userProducts: OrderProduct[] = JSON.parse(
      localStorage.getItem('user_products')
    );
    this.products.map((product) => {
      if (
        userProducts &&
        userProducts.some(
          (user_product: OrderProduct) =>
            user_product.product._id === product.product._id
        )
      ) {
        product.user_quantity =
          this.getProductById(userProducts, product.product._id)
            ?.user_quantity ?? 0;
      } else {
        product.user_quantity = 0;
      }
    });
  }

  gotoCart(): void {
    const cart = this.getUserProducts();
    if (cart.length === 0) {
      this.toast.generateToast('info', 'Information', 'Votre panier est vide');
    } else {
      localStorage.setItem('user_products', JSON.stringify(cart));
      this.router.navigate(['cart']);
    }
  }

  getUserProductsCount(): number {
    const userProducts = this.getUserProducts();
    return Object.values(userProducts).reduce(
      (t, { user_quantity }) => t + user_quantity,
      0
    );
  }

  getUserProducts(): Product[] {
    return this.products.filter((product) => product.user_quantity > 0);
  }

  addQuantity(product: Product): void {
    product.user_quantity++;
    localStorage.setItem(
      'user_products',
      JSON.stringify(this.getUserProducts())
    );
  }

  removeQuantity(product: Product): void {
    if (product.user_quantity > 0) {
      product.user_quantity--;
      localStorage.setItem(
        'user_products',
        JSON.stringify(this.getUserProducts())
      );
    }
  }
  getProductsByCat(category: string): OrderProduct[] {
    return this.products.filter(
      (product) => product.product.category === category
    );
  }

  getAllCategories(restaurant_id: string): void {
    this.productService.getCategories(restaurant_id).subscribe(
      (data: ProductCategory[]) => {
        this.product_categories = data;
        this.swipe.tab_length = this.product_categories.length;
      },
      () => {
        this.error = true;
        this.loading = false;
      }
    );
  }

  tabClick(tab: any): void {
    this.selectedTab = tab.index;
  }
  onSwipe(e: TouchEvent, when: string): void {
    this.swipe.apply(e, when, this.selectedTab);
    this.selectedTab = this.swipe.selectedTab;
  }
}
