import { Component, OnInit } from '@angular/core';
import { TableService } from 'src/app/shared/services/table.service';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.scss'],
})
export class HeaderNavComponent implements OnInit {
  public isCollapsed: boolean;
  constructor(public tableService: TableService) {}

  ngOnInit(): void {
    this.isCollapsed = true;
  }
}
