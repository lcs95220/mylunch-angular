import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-goback',
  templateUrl: './goback.component.html',
  styleUrls: ['./goback.component.scss'],
})
export class GobackComponent implements OnInit {
  @Input() title: string = '';
  @Input() path: string = '';

  constructor(private location: Location, private router: Router) {}

  goBack(): void {
    if (this.path) {
      this.router.navigate([this.path]);
    } else {
      this.location.back();
    }
  }

  ngOnInit(): void {}
}
