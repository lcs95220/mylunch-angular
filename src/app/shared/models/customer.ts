export class Customer {
  constructor(
    public username: string,
    public tableId: string,
    public restaurantId: string,
    public _id?: string,
    public master?: boolean
  ) {}
}
