import { OrderProduct } from './order-product';

export class Order {
  constructor(
    public _id?: string,
    public table?: string,
    public restaurant?: string,
    public username?: string,
    public products?: OrderProduct[],
    public status?: string,
    public createdAt?: string,
    public updatedAt?: string
  ) {}
}
