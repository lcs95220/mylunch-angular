export class Product {
  constructor(
    public _id?: string,
    public title?: string,
    public description?: string,
    public restaurant?: string,
    public category?: string,
    public image_path?: string,
    public price?: number,
    public quantity?: number,
    public user_quantity?: number,
    public createdAt?: string,
    public updatedAt?: string
  ) {}
}
