import { Customer } from './customer';

export class Table {
  constructor(
    public _id?: string,
    public min?: number,
    public max?: number,
    public current?: number,
    public available?: boolean,
    public restaurant?: string,
    public customers?: Customer
  ) {}
}
