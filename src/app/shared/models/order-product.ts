import { Product } from 'src/app/shared/models/product';

export class OrderProduct {
  constructor(public product: Product, public user_quantity: number = 0) {}
}
