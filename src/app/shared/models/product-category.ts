export class ProductCategory {
  constructor(
    public _id?: string,
    public name?: string,
    public restaurant?: string
  ) {}
}
