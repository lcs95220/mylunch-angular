import { Order } from './../models/order';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from 'config';
import { OrderProduct } from 'src/app/shared/models/order-product';
import { Customer } from 'src/app/shared/models/customer';
import { TableService } from '../services/table.service';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  private BASE_URL: string = `${config.BASE_API_URL}/order/`;
  private customer: Customer;
  constructor(private http: HttpClient, private tableService: TableService) {
    this.customer = this.tableService.getUser();
  }
  createOrder(products: OrderProduct[]) {
    return this.http.put(this.BASE_URL + this.customer.tableId, {
      username: this.customer.username,
      products,
    });
  }

  declineOrder(order: Order) {
    return this.http.put(`${this.BASE_URL}decline/${this.customer.tableId}`, {
      username: this.customer.username,
      order_id: order._id,
    });
  }

  cancelOrder(order: Order) {
    return this.http.put(`${this.BASE_URL}cancel/${this.customer.tableId}`, {
      username: this.customer.username,
      order_id: order._id,
    });
  }

  getAllOrders() {
    return this.http.post(this.BASE_URL + this.customer.tableId, {
      username: this.customer.username,
    });
  }

  getAllOrdersOfTable() {
    return this.http.post(`${this.BASE_URL}master/${this.customer.tableId}`, {
      username: this.customer.username,
    });
  }
}
