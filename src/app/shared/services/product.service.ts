import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { config } from 'config';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private BASE_URL: string = `${config.BASE_API_URL}/product/`;

  constructor(private http: HttpClient) {}

  getCategories(restaurant_id: string): Observable<any> {
    return this.http.get(this.BASE_URL + 'category/' + restaurant_id);
  }
  getAllProducts(restaurant_id: string): Observable<any> {
    return this.http.get(this.BASE_URL + restaurant_id);
  }
}
