import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { config } from 'config';
import { Customer } from 'src/app/shared/models/customer';

@Injectable({
  providedIn: 'root',
})
export class TableService {
  private customer: Customer;
  private BASE_URL: string = `${config.BASE_API_URL}/table/`;
  constructor(private http: HttpClient) {
    this.customer = this.getUser();
  }

  userLoggedIn(): boolean {
    if (this.getUser()) {
      return true;
    }
    return false;
  }

  setUser(username: string, tableId: string, restaurantId: string): boolean {
    if (username && tableId && restaurantId) {
      localStorage.setItem('username', username.trim().toLowerCase());
      localStorage.setItem('restaurant_id', restaurantId);
      localStorage.setItem('table_id', tableId);
      return true;
    }
    return false;
  }

  getUser() {
    const username = localStorage.getItem('username');
    const restaurantId = localStorage.getItem('restaurant_id');
    const tableId = localStorage.getItem('table_id');
    if (username && restaurantId && tableId) {
      return new Customer(username, tableId, restaurantId);
    } else return null;
  }

  checkDisponibility(tableId: string): Observable<any> {
    return this.http.get(this.BASE_URL + 'disponibility/' + tableId);
  }

  joinTable(tableId: string, username: string): Observable<any> {
    return this.http.post(this.BASE_URL + 'join/' + tableId, { username });
  }

  cleanStorage() {
    localStorage.removeItem('username');
    localStorage.removeItem('table_id');
    localStorage.removeItem('restaurant_id');
    localStorage.removeItem('user_products');
    this.customer = null;
  }

  getCustomerInfo(): Observable<any> {
    this.customer = this.getUser();
    return this.http.post(
      `${this.BASE_URL}customer/info/${this.customer.tableId}`,
      {
        username: this.customer.username,
      }
    );
  }
}
