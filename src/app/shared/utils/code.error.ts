export let CODE_ERROR = {
  TABLE_NOT_FOUND: 'Cette table semble ne pas exister',
  TABLE_USERNAME_EXISTS: "Ce nom d'utilisateur existe déjà",
  TABLE_OR_USERNAME_DOES_NOT_EXIST:
    "Il semble que vous n'ayez pas accès à cette table",
  TABLE_CLOSED: 'Cette table est actuellement fermée',
  TABLE_MAX_CAPACITY: 'Cette table a atteint sa capacité maximale',
  BODY_INPUTS_INVALID: 'Des champs ne correspondent pas',
  OTHER_ERROR: 'Oops, erreur inconnue, veuillez contacter le service client',
};
