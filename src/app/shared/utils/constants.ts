export let constants = {
  APP_TITLE: 'MyLunch',
  ORDER_STATUS: {
    accepted: {
      message: 'Vu en cuisine',
      icon: 'done_all',
    },
    sent: {
      message: 'Envoyée en cuisine',
      icon: 'done',
    },
    waiting: {
      message: 'En attente du responsable de la table',
      icon: 'timer',
    },
    declined: {
      message: 'Refusée',
      icon: 'close',
    },
  },
};
