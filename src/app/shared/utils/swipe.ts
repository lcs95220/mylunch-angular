export class Swipe {
  public selectedTab: number = 0;
  public tab_length: number = 0;
  private swipeCoord: [number, number];
  private swipeTime: any;

  constructor() {}
  apply(e: TouchEvent, when: string, selectedTab: number): void {
    this.selectedTab = selectedTab;
    const coord: [number, number] = [
      e.changedTouches[0].clientX,
      e.changedTouches[0].clientY,
    ];
    const time = new Date().getTime();
    if (when === 'start') {
      this.swipeCoord = coord;
      this.swipeTime = time;
    } else if (when === 'end') {
      const direction = [
        coord[0] - this.swipeCoord[0],
        coord[1] - this.swipeCoord[1],
      ];
      const duration = time - this.swipeTime;
      if (
        duration < 1000 &&
        Math.abs(direction[0]) > 30 &&
        Math.abs(direction[0]) > Math.abs(direction[1] * 3)
      ) {
        const swipe = direction[0] < 0 ? 'next' : 'previous';
        if (swipe === 'next') {
          const isFirst = this.selectedTab === 0;
          if (this.selectedTab < this.tab_length - 1) {
            this.selectedTab = isFirst ? 1 : this.selectedTab + 1;
          } else {
            this.selectedTab = 0;
          }
          window.scrollTo(0, 0);
        } else if (swipe === 'previous') {
          if (this.selectedTab === 0) {
            this.selectedTab = this.tab_length - 1;
          } else if (this.selectedTab >= 1) {
            this.selectedTab = this.selectedTab - 1;
          }
          window.scrollTo(0, 0);
        }
      }
    }
  }
}
