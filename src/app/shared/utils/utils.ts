import { CODE_ERROR } from './code.error';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2/dist/sweetalert2.js';

export function checkMobile(): boolean {
  const ua = navigator.userAgent;

  if (
    /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini|CriOS/i.test(ua)
  )
    return true;
  else return false;
}

export function generateSwal(title: string, type: string, position?: string) {
  if (!!position) {
    position = 'top-end';
    if (checkMobile()) {
      position = 'center';
    }
  }
  let options = {
    position: position,
    icon: type,
    title: title,
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast: any) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    },
  };
  Swal.fire(options);
}

export class Toast {
  constructor(private toaster: ToastrService) {}
  generateToast(type: string, title: string, body: string): void {
    switch (type) {
      case 'success':
        this.toaster.success(body, title);
        break;
      case 'error':
        this.toaster.error(body, title);
        break;
      case 'warning':
        this.toaster.warning(body, title);
        break;
      default:
        this.toaster.info(body, title);
        break;
    }
  }
}

export function handleError(message: string | object): string {
  if (typeof message === 'string') {
    const errorMessage = CODE_ERROR[message] ?? CODE_ERROR.OTHER_ERROR;
    return errorMessage;
  }
  const messageCode = message['code'];
  if (messageCode != undefined) {
    return CODE_ERROR[message['code']] ?? CODE_ERROR.OTHER_ERROR;
  }
  return CODE_ERROR.OTHER_ERROR;
}
