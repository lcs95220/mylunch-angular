import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { checkMobile } from './utils';

export function generateSwal(title: string, type: string, position?: string) {
  if (!!position) {
    position = 'top-end';
    if (checkMobile()) {
      position = 'center';
    }
  }
  let options = {
    position: position,
    icon: type,
    title: title,
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast: any) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    },
  };
  Swal.fire(options);
}

export class Toast {
  constructor(private toaster: ToastrService) {}
  generateToast(type: string, title: string, body: string): void {
    switch (type) {
      case 'success':
        this.toaster.success(body, title);
        break;
      case 'error':
        this.toaster.error(body, title);
        break;
      case 'warning':
        this.toaster.warning(body, title);
        break;
      default:
        this.toaster.info(body, title);
        break;
    }
  }
}
