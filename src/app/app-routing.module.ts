import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { TableComponent } from './views/table/table.component';
import { ContactComponent } from './views/contact/contact.component';
import { CartComponent } from './views/cart/cart.component';
import { CustomerGuard } from './shared/guards/customer.guard';
import { NotAlreadySignedGuard } from './shared/guards/not-already-signed.guard';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'contact', component: ContactComponent },
  {
    path: 'table/:id',
    component: TableComponent,
    canActivate: [NotAlreadySignedGuard],
  },
  { path: 'cart', component: CartComponent, canActivate: [CustomerGuard] },
  {
    path: 'card',
    canActivate: [CustomerGuard],
    loadChildren: () =>
      import('src/app/views/card/card.module').then((m) => m.CardModule),
  },
  {
    path: 'order',
    canActivate: [CustomerGuard],
    loadChildren: () =>
      import('src/app/views/order/order.routing').then(
        (m) => m.OrderRoutingModule
      ),
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
